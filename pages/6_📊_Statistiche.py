###Librerie
from PIL import Image
import pandas as pd
import plotly.express as px
import streamlit as st
import numpy as np
import PyPDF2
import pandas as pd
import os
import re
from collections import Counter
import json
from streamlit_lottie import st_lottie
import function as RagFunction 
from wordcloud import WordCloud, STOPWORDS
import matplotlib.pyplot as plt

def count_pages_in_pdf(pdf_path):
  """Counts the number of pages in a PDF file.

  Args:
      pdf_path (str): The path to the PDF file.

  Returns:
      int: The number of pages in the PDF file.
  """
  with open(pdf_path, 'rb') as f:
    reader = PyPDF2.PdfReader(f)
    return len(reader.pages)
def count_pages_in_folder(folder_path):
  """Counts the number of pages in each PDF file in a given folder and creates a DataFrame.

  Args:
      folder_path (str): The path to the folder containing PDF files.

  Returns:
      pandas.DataFrame: A DataFrame with columns 'Filename' and 'Page Count'.
  """
  data = []
  for filename in os.listdir(folder_path):
    if filename.endswith('.pdf'):
      file_path = os.path.join(folder_path, filename)
      page_count = count_pages_in_pdf(file_path)
      data.append({'Nome File': filename, 'Numero Pagine': page_count})
  df = pd.DataFrame(data)
  return df

def save_to_csv(df, csv_path):
  """Saves a pandas DataFrame to a CSV file.

  Args:
      df (pandas.DataFrame): The DataFrame to save.
      csv_path (str): The path to the CSV file.
  """
  if not os.path.exists(csv_path):
    df.to_csv(csv_path, index=False)
  else:
    print(f"CSV file '{csv_path}' already exists. Skipping saving.")


def extract_text_from_pdf(pdf_path):
  """Extracts text from a PDF file.

  Args:
      pdf_path (str): The path to the PDF file.

  Returns:
      str: The extracted text from the PDF file.
  """
  with open(pdf_path, 'rb') as f:
    reader = PyPDF2.PdfReader(f)
    page_count = len(reader.pages)
    text = ''
    for page_num in range(page_count):
      page = reader.pages[page_num]
      text += page.extract_text()
  return text

def gen_wordcloud(stopwords):

    text = ""

    for filename in os.listdir(folder_path):
        if filename.endswith('.pdf'):
            file_path = os.path.join(folder_path, filename)
            text += extract_text_from_pdf(file_path)

    wordCloud = WordCloud(width=1400, height=800, max_words=80, max_font_size=300, stopwords = stopwords, min_word_length=3).generate(text) 
    
    plt.figure(figsize=(15,15))
    plt.imshow(wordCloud.recolor( colormap= 'viridis' , random_state=17), interpolation="bilinear")
    plt.axis('off')
    plt.savefig('./WC.jpg') 
    Image.open("./WC.jpg") 
    return "./WC.jpg" 

def count_words_in_folder(folder_path):
  """Counts the most frequent words in PDF files in a given folder.

  Args:
      folder_path (str): The path to the folder containing PDF files.

  Returns:
      pandas.DataFrame: A DataFrame with columns 'Word' and 'Count'.
  """

  word_counts = Counter()
  for filename in os.listdir(folder_path):
    if filename.endswith('.pdf'):
      file_path = os.path.join(folder_path, filename)
      text = extract_text_from_pdf(file_path)
      words = [word for word in text.lower().split() if re.match(r"^\w+$", word)]
      word_counts.update(words)
  # Create a DataFrame from Counter
  df_word_counts = pd.DataFrame.from_dict(word_counts, orient='index', columns=['Count'])
  # Sort DataFrame by count in descending order
  df_word_counts = df_word_counts.sort_values(by='Count', ascending=False)
  # Reset index to get proper word order
  df_word_counts = df_word_counts.reset_index().rename(columns={'index': 'Word'})
  return df_word_counts


###Informazioni Pagina
st.set_page_config(
    page_title="Statistiche",
    page_icon="📊",
    layout="wide"
)

RagFunction.useCss("./style.css")
st.title("Statistiche dei documenti 📊")

folder_path = "documents"
analysis_folder = "analisi"
csv_path = os.path.join(analysis_folder, "page_counts.csv")

#inserisco nella sidebar un radiobutton per scegliere se visualizzare le statistiche dei documenti caricati o dei documenti di esempio
st.sidebar.title("Scegli i documenti da analizzare")
documenti=st.sidebar.radio("Documenti",["Documenti esempio","Documenti caricati"])

stopwords_ita = [
            'a', 'abbiamo', 'ad', 'adesso', 'ai', 'al', 'allora', 'alla', 'allo', 'anche', 'ancora', 
            'avere', 'aveva', 'avevamo', 'avevano', 'avevate', 'avevo', 'ben', 'bene', 'brava', 'bravo', 
            'cosa', 'cui', 'che', 'chi', 'ci', 'come', 'con', 'così', 'da', 'dare', 'dei', 'del', 
            'della', 'delle', 'dello', 'deve', 'devo', 'di', 'dice', 'diciamo', 'dicono', 'dire', 
            'dov\'è', 'dovrebbe', 'dovrebbero', 'dovrei', 'dovremo', 'dovreste', 'dovresti', 'dovuto', 
            'dove', 'e', 'è', 'era', 'erano', 'essere', 'fa', 'fanno', 'fare', 'fatto', 'favore', 
            'fine', 'fino', 'fra', 'gli', 'ha', 'hai', 'hanno', 'ho', 'i', 'il', 'in', 'io', 'là', 
            'la', 'le', 'lei', 'li', 'lo', 'loro', 'lui', 'ma', 'me', 'meglio', 'molta', 'molti', 
            'molto', 'ne', 'nella', 'nelle', 'nello', 'no', 'noi', 'non', 'nostra', 'nostre', 'nostri', 
            'nostro', 'nulla', 'o', 'ogni', 'oltre', 'ora', 'per', 'perché', 'più', 'po', 'poi', 
            'quale', 'quali', 'qualunque', 'quando', 'quanto', 'quello', 'questo', 'qui', 'quindi', 
            'sarà', 'sarei', 'saranno', 'saremo', 'sareste', 'saresti', 'sarete', 'sei', 'sembra', 
            'sembrano', 'sembrava', 'sembravano', 'sembrerà', 'sembreranno', 'senza', 'sia', 'siamo', 
            'siete', 'solo', 'sono', 'sta', 'stai', 'stiamo', 'stiamo', 'stanno', 'stare', 'stato', 
            'su', 'sua', 'sue', 'sui', 'suo', 'suoi', 'sul', 'sulla', 'tale', 'tali', 'tanto', 'te', 
            'ti', 'tra', 'tu', 'tua', 'tue', 'tuo', 'tuoi', 'tutti', 'tutto', 'un', 'una', 'uno', 
            'va', 'vai', 'vanno', 'veniva', 'venire', 'verrà', 'vi', 'via', 'voi', 'vostra', 'vostre', 
            'vostri', 'vostro', 'nel', 'nei', 'negli', 'nelle', 'degli', 'delle', 'dallo', 'dalla', 'alle', 'si', 
            'sì', 'no', 'non', 'mai', 'neanche', 'neppure', 'nemmeno', 'sia', 'tuttavia', 'infatti', 'inoltre',
            'perciò', 'quindi', 'dunque','ed', 'o', 'ma', 'se', 'che', 'come', 'perché', 'quando', 'dove', 'oppure', 'anche', 'neanche', 'neppure', 'nemmeno', 'sia', 'tuttavia', 'infatti', 'inoltre', 'perciò', 'quindi'
]

stopwords_eng = [
            'a', 'about', 'above', 'after', 'again', 'against', 'all', 'am', 'an', 'and', 'any', 
            'are', 'aren\'t', 'as', 'at', 'be', 'because', 'been', 'before', 'being', 'below', 'between', 
            'both', 'but', 'by', 'can\'t', 'cannot', 'could', 'couldn\'t', 'did', 'didn\'t', 'do', 'does', 
            'doesn\'t', 'doing', 'don\'t', 'down', 'during', 'each', 'few', 'for', 'from', 'further', 
            'had', 'hadn\'t', 'has', 'hasn\'t', 'have', 'haven\'t', 'having', 'he', 'he\'d', 'he\'ll', 
            'he\'s', 'her', 'here', 'here\'s', 'hers', 'herself', 'him', 'himself', 'his', 'how', 'how\'s', 
            'i', 'i\'d', 'i\'ll', 'i\'m', 'i\'ve', 'if', 'in', 'into', 'is', 'isn\'t', 'it', 'it\'s', 'its', 
            'itself', 'let\'s', 'me', 'more', 'most', 'mustn\'t', 'my', 'myself', 'no', 'nor', 'not', 'of', 
            'off', 'on', 'once', 'only', 'or', 'other', 'ought', 'our', 'ours', 'ourselves', 'out', 'over', 
            'own', 'same', 'shan\'t', 'she', 'she\'d', 'she\'ll', 'she\'s', 'should', 'shouldn\'t', 'so', 
            'some', 'such', 'than', 'that', 'that\'s', 'the', 'their', 'theirs', 'them', 'themselves', 
            'then', 'there', 'there\'s', 'these', 'they', 'they\'d', 'they\'ll', 'they\'re', 'they\'ve', 
            'this', 'those', 'through', 'to', 'too', 'under', 'until', 'up', 'very', 'was', 'wasn\'t', 
            'we', 'we\'d', 'we\'ll', 'we\'re', 'we\'ve', 'were', 'weren\'t', 'what', 'what\'s', 'when', 
            'when\'s', 'where', 'where\'s', 'which', 'while', 'who', 'who\'s', 'whom', 'why', 'why\'s', 
            'with', 'won\'t', 'would', 'wouldn\'t', 'you', 'you\'d', 'you\'ll', 'you\'re', 'you\'ve', 
            'your', 'yours', 'yourself', 'yourselves', 'zero', 'one', 'two', 'three', 'four', 'five', 
            'six', 'seven', 'eight', 'nine', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
 ]



if documenti=="Documenti esempio":
    st.write("Hai scelto di visualizzare le statistiche dei documenti di esempio")

    # Create the "analisi" folder if it doesn't exist
    if not os.path.exists(analysis_folder):
        os.makedirs(analysis_folder)

    # Check if CSV file exists
    if os.path.exists(csv_path):
        print(f"CSV file '{csv_path}' already exists. Loading existing data.")
        df_page_counts = pd.read_csv(csv_path)
    else:
        # Count pages and create DataFrame
        print("Counting pages...")
        df_page_counts = count_pages_in_folder(folder_path)
        #ordino il dataframe in base al numero di pagine
        df_page_counts = df_page_counts.sort_values(by='Numero Pagine', ascending=False)

        # Save DataFrame to CSV
        print("Saving page counts to CSV...")
        save_to_csv(df_page_counts, csv_path)

    #mosto il grafico con il numero di pagine per ogni documento
    fig = px.bar(df_page_counts, x='Nome File', y='Numero Pagine', title='Numero di pagine per documento')
    st.plotly_chart(fig,use_container_width=True)

    path_analisi_parole = os.path.join(analysis_folder, "word_counts.csv")
    # Check if CSV file exists
    if os.path.exists(path_analisi_parole):
        print(f"CSV file '{path_analisi_parole}' already exists. Loading existing data.")
        df_word_counts = pd.read_csv(path_analisi_parole)
        #sommo la colonna Count per avere il numero totale di parole
        totale_parole = df_word_counts['Count'].sum()
        print(f"Numero totale di parole: {totale_parole}")
    else:
        # Count words and create DataFrame
        print("Counting words...")
        df_word_counts = count_words_in_folder(folder_path)
        #TODO Controlla se parole sono italiante

        #elimino gli articoli e le preposizioni italiane
        articoli = ['il', 'lo', 'la', 'i', 'gli', 'le', 'un', 'uno', 'una', 'dei', 'degli', 'delle', 'dello', 'della']
        preposizioni = ['di', 'a', 'da', 'in', 'con', 'su', 'per', 'tra', 'fra']
        congiunzioni = ['e', 'o', 'ma', 'se', 'che', 'come', 'perché', 'quando', 'dove', 'oppure', 'anche', 'neanche', 'neppure', 'nemmeno', 'sia', 'tuttavia', 'infatti', 'inoltre', 'perciò', 'quindi']

        df_word_counts = df_word_counts[~df_word_counts['Word'].isin(articoli)]
        df_word_counts = df_word_counts[~df_word_counts['Word'].isin(preposizioni)]      
        df_word_counts = df_word_counts[~df_word_counts['Word'].isin(congiunzioni)]
        df_word_counts = df_word_counts[~df_word_counts['Word'].isin(stopwords_ita)]
        df_word_counts = df_word_counts[~df_word_counts['Word'].isin(stopwords_eng)]

        #ordino il dataframe in base al numero di occorrenze
        df_word_counts = df_word_counts.sort_values(by='Count', ascending=False)

        # Save DataFrame to CSV
        print("Saving word counts to CSV...")
        save_to_csv(df_word_counts, path_analisi_parole)

    #mostro il grafico con le parole più frequenti

    fig_word = px.bar(df_word_counts.head(50), x='Word', y='Count', title='Parole più frequenti nei documenti')
    st.plotly_chart(fig_word,use_container_width=True)

    #mostro il wordCloud
    
    if not os.path.exists("./WC.jpg"):
        gen_wordcloud(stopwords = stopwords_ita + stopwords_eng)

    st.subheader("WordCloud generato con le parole più frequenti nei documenti")
    st.image("./WC.jpg")


else :
    st.write("Hai scelto di visualizzare le statistiche dei documenti caricati dall'utente")
    folder_path = "documents_user"
    analysis_folder = "analisi_user"
    #se la cartella documents_user non è vuota
    if not os.path.exists("documents_user"):
        os.makedirs("documents_user")
        
    if os.listdir("documents_user"):
        csv_path = os.path.join(analysis_folder, "page_counts.csv")
        # Create the "analisi" folder if it doesn't exist
        if not os.path.exists(analysis_folder):
            os.makedirs(analysis_folder)

        # Check if CSV file exists
        if os.path.exists(csv_path):
            print(f"CSV file '{csv_path}' already exists. Loading existing data.")
            df_page_counts = pd.read_csv(csv_path)
        else:
            # Count pages and create DataFrame
            print("Counting pages...")
            df_page_counts = count_pages_in_folder(folder_path)

            # Save DataFrame to CSV
            print("Saving page counts to CSV...")
            save_to_csv(df_page_counts, csv_path)

        #mosto il grafico con il numero di pagine per ogni documento
        fig7 = px.bar(df_page_counts, x='Nome File', y='Numero Pagine', title='Numero di pagine per documento')
        st.plotly_chart(fig7)

        path_analisi_parole = os.path.join(analysis_folder, "word_counts.csv")
        # Check if CSV file exists
        if os.path.exists(path_analisi_parole):
            print(f"CSV file '{path_analisi_parole}' already exists. Loading existing data.")
            df_word_counts = pd.read_csv(path_analisi_parole)
        else:
            # Count words and create DataFrame
            print("Counting words...")
            df_word_counts = count_words_in_folder(folder_path)
            #TODO Controlla se parole sono italiante

            #elimino gli articoli e le preposizioni italiane
            # articoli = ['il', 'lo', 'la', 'i', 'gli', 'le', 'un', 'uno', 'una', 'dei', 'degli', 'delle', 'dello', 'della']
            # preposizioni = ['di', 'a', 'da', 'in', 'con', 'su', 'per', 'tra', 'fra']
            # congiunzioni = ['e', 'o', 'ma', 'se', 'che', 'come', 'perché', 'quando', 'dove', 'oppure', 'anche', 'neanche', 'neppure', 'nemmeno', 'sia', 'tuttavia', 'infatti', 'inoltre', 'perciò', 'quindi', 'dunque' ]
            # df_word_counts = df_word_counts[~df_word_counts['Word'].isin(articoli)]
            # df_word_counts = df_word_counts[~df_word_counts['Word'].isin(preposizioni)]      
            # df_word_counts = df_word_counts[~df_word_counts['Word'].isin(congiunzioni)]


            # Save DataFrame to CSV
            print("Saving word counts to CSV...")
            save_to_csv(df_word_counts, path_analisi_parole)

        #mostro il grafico con le parole più frequenti
        fig_word2 = px.bar(df_word_counts.head(50), x='Word', y='Count', title='Parole più frequenti nei documenti')
        st.plotly_chart(fig_word2)
    else:
       st.write("Non sono stati caricati documenti dall'utente")
       with open("animazioni/animazione_non_trovato.json") as f:
        animazioneNonTrovato = json.load(f)
        st_lottie(animazioneNonTrovato , speed=1, height=600, key="initial")