import pandas as pd
import plotly.express as px
import streamlit as st
from PIL import Image
from rag import LLMQuestionManager
from streamlit_pdf_viewer import pdf_viewer
from streamlit_lottie import st_lottie
import json
import fitz  # PyMuPDF
import os
import textwrap
import function as RagFunction

if not os.path.exists("temporaneiDocumenti"):
    os.makedirs("temporaneiDocumenti")




basepath = "./documents"
###Informazioni Pagina
st.set_page_config(
    page_title="Visualizza le fonti",
    page_icon="🤖",
    layout="wide"
)


RagFunction.useCss("./style.css")
RagFunction.useCss("./styleFonti.css")
st.title("📋 Visualizza le fonti")

with open("animazioni/animazione_ricerca_fonti.json") as f:
    animazione = json.load(f)
with open("animazioni/animazione_non_trovato.json") as f:
    animazioneNonTrovato = json.load(f)

if "fonti-disponibili" not in st.session_state:
    st.session_state["fonti-disponibili"] = False


if not st.session_state["fonti-disponibili"]:
    st.write("Non sono state trovate fonti")
    st_lottie(animazioneNonTrovato , speed=1, height=600, key="initial")
else:
    st_lottie(animazione , speed=1, height=200, key="initial")
    st.write("Fonti disponibili")
    data=st.session_state.fonti

    #estrai fonti da data
    fonti=data["fonti"]


    # Estrai tutti i valori della chiave "documento" in una lista
    documenti = [entry["documento"] for entry in data["fonti"]]
    #elimino eventuali duplicati
    documenti = list(set(documenti))
    #replace documents/ con "" per avere solo il nome del file
    documentiSelezione = [doc.replace("documents/", "") for doc in documenti]
    #dalla lista voglio una stringa di tutti i documenti concatenati dalla virgola
    #documentiSelezione = ", ".join(documentiSelezione)


    st.write("Ultima risposta:  ",data["risposta"])
    st.write("La risposta dell'ultimo messaggio e stata generata dalle seguenti fonti:", str(documentiSelezione))


    option = st.selectbox(
    "Seleziona fonte da visualizzare",
    (documentiSelezione))


    optionConPath = basepath+"/"+option
    optionConPath = optionConPath.replace("./", "")
    testi = [entry["testo"] for entry in data["fonti"] if entry["documento"] == optionConPath]
    testi = list(testi)
    pagine = [entry["pagina"] for entry in data["fonti"] if entry["documento"] == optionConPath]
    #trasformo la lista di pagine in interi
    pagine = list(map(int, pagine))
    pagineUniche = list(set(pagine))
    stringaPagine = ", ".join(map(str, pagineUniche))
    st.write("Del documento sono stati trovati riscontri alle seguenti pagine:  ", stringaPagine)


            
    path=basepath+"/"+option

    output_path= path.replace("documents", "temporaneiDocumenti")
    if os.path.exists(output_path):
        os.remove(output_path)

    pdf_document = fitz.open(path)

    for fonte in fonti:
        if fonte["documento"]==optionConPath:
            testo=fonte["testo"]
            numero_pagina=fonte["pagina"]
            page = pdf_document[numero_pagina]
            # Estrai il testo dalla pagina
            text = page.get_text()
            # Se la frase da evidenziare è presente nella pagina
            # Cerca e evidenzia la frase
            text_instances = page.search_for(testo)
            for inst in text_instances:
                highlight = page.add_highlight_annot(inst)
                highlight.update()


    pdf_document.save(output_path)
    # Chiudi il PDF
    pdf_document.close()


    #visualizzo al centro il pdf utilizzando le 3 colonne
    col1, col2, col3 = st.columns([2, 6, 2])
    with col2:
        pdf_viewer(input=output_path, height=800)
        namefile=output_path.replace("temporaneiDocumenti/", "")
        st.download_button(label="Clicca qui per scaricare il PDF", data=open(output_path, 'rb').read(), file_name=namefile, mime="application/pdf")
            

