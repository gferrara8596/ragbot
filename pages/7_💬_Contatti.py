###Librerie
import datetime as dt
import pandas as pd
import plotly.express as px
import streamlit as st
import numpy as np
from PIL import Image
import time
import function as RagFunction


###Informazioni Pagina
st.set_page_config(
    page_title="Contatti",
    page_icon="📑",
    layout="wide"
)

with open('style.css', 'r') as f:
    css = f.read()
with open('styleContatti.css', 'r') as f:
    cssContatti = f.read()



RagFunction.useCss("./style.css")

RagFunction.useCss("./styleContatti.css")

st.title("Contatti")

imageC = Image.open('./static/Contatti.png')


st.image(imageC, width=800)


st.markdown("""
<a href="https://github.com/ClaudioDotani/Progetto-Text-Mining-Chatbot" target="_blank">
  <img src="app/static/BOTTONEGITHUB2.png" alt="Click me" id="my-github-button">
</a>
""", unsafe_allow_html=True)



#st.markdown(css_prova, unsafe_allow_html=True)
while True:
    time.sleep(4)
    st.balloons()
    st.balloons()
