import streamlit as st
from rag import LLMQuestionManager
import json
import os
from gtts import gTTS
from streamlit_mic_recorder import speech_to_text
import requests
import function as RagFunction
from streamlit_lottie import st_lottie
from streamlit_lottie import st_lottie_spinner

def manual_switch_mic_recorder():
        """Record audio from the microphone."""
        red_square = "\U0001F7E5"
        microphone = "\U0001F3A4"
        play_button = "\U000025B6"

        text = speech_to_text(
            language='it',
            start_prompt=play_button + microphone,
            stop_prompt=red_square,
            just_once=False,
            use_container_width=True,
            callback=stt_callback,
            args=(),
            kwargs={},
            key="stt_google"
        )

def executePrompt(prompt):
    #client = OpenAI(api_key=openai_api_key)
    st.session_state.messages.append({"role": "user", "content": prompt})
    st.chat_message("user",avatar="static/user.png").write(prompt)
    msg=questionManager.ask_to_llm(prompt)
    dati=json.loads(msg)
    risposta = dati["risposta"]
    st.session_state.messages.append({"role": "assistant", "content": risposta})
    st.session_state['fonti-disponibili'] = True
    
    #controller.set('fonti-trovate', msg)
    st.session_state['fonti'] = dati
    
    #controller.set('fonti-trovate', )
    st.chat_message("assistant",avatar="static/EVE.png").write(risposta)
    #Sintesi Vocale
    tts = gTTS(text=risposta, lang='it')
    tts.save('audio.mp3')
    audio_file = open('audio.mp3','rb')
    audio_bytes = audio_file.read()
    st.audio(audio_bytes, format='audio/mp3')
    audio_file.close()
    os.remove('audio.mp3')

def executePromptEleven(prompt, chiaveEleven):
    CHUNK_SIZE = 1024
     #client = OpenAI(api_key=openai_api_key)
    st.session_state.messages.append({"role": "user", "content": prompt})
    st.chat_message("user",avatar="static/user.png").write(prompt)
    msg=questionManager.ask_to_llm(prompt)
    dati=json.loads(msg)
    risposta = dati["risposta"]
    st.session_state.messages.append({"role": "assistant", "content": risposta})
    st.session_state['fonti-disponibili'] = True
    
    #controller.set('fonti-trovate', msg)
    st.session_state['fonti'] = dati
    
    #controller.set('fonti-trovate', )
    st.chat_message("assistant",avatar="static/EVE.png").write(risposta)

    #Sintesi Vocale
    
    url = "https://api.elevenlabs.io/v1/text-to-speech/Q16GS16HFFxkZ6ZHEX2v"

    headers = {
    "Accept": "audio/mpeg",
    "Content-Type": "application/json",
    "xi-api-key": chiaveEleven
    }
    data = {
        "text": risposta,
        "model_id": "eleven_multilingual_v2",
        "voice_settings": {
        "stability": 0.5,
        "similarity_boost": 0.75
        }
    }
    response = requests.post(url, json=data, headers=headers)

    with open('output.mp3', 'wb') as f:
        for chunk in response.iter_content(chunk_size=CHUNK_SIZE):
            if chunk:
                f.write(chunk)
    st.audio("output.mp3", format='audio/mp3')

def stt_callback():
 if st.session_state.stt_google_output:
    st.session_state.sttPrompt = st.session_state.stt_google_output
    st.rerun()

if "sttPrompt" not in st.session_state:
    st.session_state.sttPrompt = None

if "chiaveEleven" not in st.session_state:
    st.session_state["chiaveEleven"] = None

if "sintetizzatore" not in st.session_state:
    st.session_state["sintetizzatore"] = "Google"

if "loadllm" not in st.session_state:
    st.session_state["loadllm"] = False
if "questionManager" not in st.session_state:
    st.session_state["questionManager"] = None
###Informazioni Pagina
st.set_page_config(
    page_title="ChatBot",
    page_icon="🤖",
    layout="wide"
)

RagFunction.useCss("./style.css")

st.title("💬 RAGBot")
st.caption("🚀 A streamlit chatbot powered by RAGuys")




#lista dei documenti presenti in documents con path relativo

documenti = os.listdir("documents")

#ad ogni documento aggiungo il path relativo
documenti = ["documents/" + doc for doc in documenti]

with st.sidebar:
    st.write("🔊 Seleziona il sintetizzatore vocale")
    sintetizzatore=st.sidebar.radio("Sintetizzatore",["Google","Voce Clonata"])
    st.session_state["sintetizzatore"] = sintetizzatore

    if st.session_state.sintetizzatore=="Voce Clonata":
        st.write("🔑 Inserisci la chiave per api ElevenLabs")
        chiaveEleven = st.text_input("Inserisci chiave", type="password")
        if chiaveEleven:
            st.session_state["chiaveEleven"] = chiaveEleven
    
    st.sidebar.markdown("<hr>",unsafe_allow_html=True)
    st.write("🎙️Usa il microfono per interagire col chatbot")
    audio = manual_switch_mic_recorder()

with open("animazioni/caricamentoBot.json") as f:
    caricamento = json.load(f)
questionManager = None


if st.session_state.loadllm:
    
    questionManager = st.session_state.questionManager

if not st.session_state.loadllm:
    with st_lottie_spinner(caricamento,height=600,key="caricamento"):
        questionManager = LLMQuestionManager(files=documenti)
        st.session_state.questionManager = questionManager
        st.session_state.loadllm = True

if "messages" not in st.session_state:
    st.session_state["messages"] = [{"role": "assistant", "content": "Ciao sono RAGBot, come posso aiutarti?"}]
for msg in st.session_state.messages:
    if msg["role"]=="assistant":
        ava=("static/EVE.png")
    if msg["role"]=="user":
        ava=("static/user.png")
    
    with st.chat_message(msg["role"],avatar=ava):
        st.write(msg["content"])


if st.session_state.sintetizzatore=="Google":
    
    if prompt := st.chat_input(placeholder="Scrivi la tua richiesta..."):
        executePrompt(prompt)

    if prompt := st.session_state.sttPrompt:
        st.session_state.sttPrompt = None
        executePrompt(prompt)
if st.session_state.sintetizzatore=="Voce Clonata":
    if st.session_state.chiaveEleven is not None:
        chiaveEleven = st.session_state.chiaveEleven
        if prompt := st.chat_input(placeholder="Scrivi la tua richiesta..."):
            executePromptEleven(prompt,chiaveEleven)

        if prompt := st.session_state.sttPrompt:
            st.session_state.sttPrompt = None
            executePromptEleven(prompt,chiaveEleven)
    else:
        st.error("🔑 Inserisci prima la chiave per api ElevenLabs")

