import streamlit as st
from rag import LLMQuestionManager
import json
import os
from gtts import gTTS
import zipfile
import time
import function as RagFunction

def zip_folder(folder_path, zip_path):
    """
    Comprime una cartella in un file zip.
    
    Args:
        folder_path (str): Percorso della cartella da comprimere.
        zip_path (str): Percorso del file zip risultante.
    """
    with zipfile.ZipFile(zip_path, 'w', zipfile.ZIP_DEFLATED) as zipf:
        for root, dirs, files in os.walk(folder_path):
            for file in files:
                file_path = os.path.join(root, file)
                zipf.write(file_path, os.path.relpath(file_path, folder_path))
    
    st.session_state["downloadDisponibile"] = True

def extract_zip(zip_file, target_folder):
    """
    Estrae un file zip in una cartella di destinazione.

    Args:
        zip_file (BytesIO): Il contenuto del file zip.
        target_folder (str): Il percorso della cartella di destinazione.
    """
    with zipfile.ZipFile(zip_file, 'r') as zip_ref:
        zip_ref.extractall(target_folder)

def delete_documents_user():
    """
    Elimina tutti i file contenuti nella cartella documents_user.
    """
    if os.listdir("documents_user"):
        with st.spinner("Eliminazione dei documenti in corso..."):
            time.sleep(1)    
            for file in os.listdir('documents_user'):
                os.remove('documents_user/'+file)
            st.success("I documenti sono stati eliminati correttamente")
    else:
        st.warning("Non ci sono documenti da eliminare")


# def delete_embedding_user():
#     """
#     Elimina tutti i file contenuti nella cartella embedding_user.
#     """
#     if os.listdir("embedding_user"):
#         with st.spinner("Eliminazione dell'embedding in corso..."):
#             time.sleep(1)    
#             for file in os.listdir('embedding_user'):
#                 os.remove('embedding_user/'+file)
#             st.success("L'embedding è stato eliminato correttamente")
#     else:
#         st.warning("Non ci sono file da eliminare")

def download_embedding(file_zip_risultante):
        st.download_button(
                    label="Download embedding",
                    data=open(file_zip_risultante, 'rb').read(),
                    file_name="embedding_user.zip",
                    mime="application/zip"
                )

RagFunction.useCss("./style.css")   


if os.path.exists("documents_user") == False:
    os.makedirs("documents_user")

if "downloadDisponibile" not in st.session_state:
    st.session_state["downloadDisponibile"] = False


st.title("ChatBot con base di conoscenza personalizzata")
st.markdown("Questa pagina è utilizzata per caricare i documenti come base di conoscenza personalizzata per il chatbot.")



value_cs = 1000 #Valore predefinitio chunck_size
value_co = 200 #valore predefinito chunck_overlap

if 'loadEmbedding' not in st.session_state:
    st.session_state["loadEmbedding"] = False


with st.expander("Inserisci i documenti come base di conoscenza personalizzata"):
    with st.form("document_input"):
        
        document = st.file_uploader(
            "Documenti della base di conoscenza", type=".pdf", help="file .pdf", accept_multiple_files= True
        )

        row_1 = st.columns([2, 1])
        
        with row_1[0]:
            chunk_size = st.number_input(
                "Chunk Size", value=value_cs, min_value=0, step=1,
            )
        
        with row_1[1]:
            chunk_overlap = st.number_input(
                "Chunk Overlap", value=value_co, min_value=0, step=1,
            )
        
        row_2 = st.columns(2)
        
        save_button = st.form_submit_button("Esegui Embedding e Carica ChatBot🤖")
        
        
        if save_button:
            
            # Read the uploaded file
            if document:
                for file in document:
                    if file.type == "application/pdf":
                        with open('documents_user/'+file.name, 'wb') as f:
                            f.write(file.getvalue())
                    elif document == None:
                        st.error("Attenzione, i file caricati devono avere estensione .pdf o .txt")

            st.session_state["loadEmbedding"] = True
            st.session_state["persistentDirectory"] = "embedding_user"
    
    riga = st.columns([2, 1])
    with riga[0]:
        st.write("Per eliminare i documenti caricati in precedenza, clicca sul bottone di seguito")

    with riga[1]:
        if st.button("Elimina documenti ed Embedding", type="primary"):
            delete_documents_user()
            
            
            
            
            
            
            
            

            
with st.expander("Inserisci l'embedded creato in precedenza"):
    st.write("Se hai già creato l'embedding in precedenza, puoi caricarlo qui")
    # Imposta la cartella di destinazione
    target_folder = "embedding_user_upload"
    load_embedding = st.file_uploader(
        "Carica l'embedding", type=".zip", help="file .zip"
    )
    if load_embedding:
        if not os.path.exists(target_folder):
            os.makedirs(target_folder)
            extract_zip(load_embedding, target_folder)

    
        # Visualizza un messaggio di conferma
        st.success("Il file ZIP è stato estratto correttamente nella cartella embedding_user_upload.")
        st.session_state["loadEmbedding"] = True
        st.session_state["persistentDirectory"] = "embedding_user_upload"
        load_embedding = None


if st.session_state["loadEmbedding"]:   
    with st.expander("Scarica embedding dei doumenti caricati"):
            st.write("Clicca sul bottone per scaricare l'embedding dei documenti caricati")
            # Esempio di utilizzo
            cartella_da_comprimere = "embedding_user"
            file_zip_risultante = "embedding_user.zip"


            zip_folder(cartella_da_comprimere, file_zip_risultante)
            if st.session_state.downloadDisponibile:
                download_embedding(file_zip_risultante)


    documenti = os.listdir("documents_user/")
    documenti = ["documents_user/" + doc for doc in documenti]   
    directoryPersistance=st.session_state["persistentDirectory"]

    

    st.caption("🚀 A streamlit chatbot powered by RAGuys")

    questionManagerP = LLMQuestionManager(files=documenti, chunk_size=chunk_size, chunk_overlap=chunk_overlap,persistent_directory=directoryPersistance)

    if "messagesU" not in st.session_state:
        st.session_state["messagesU"] = [{"role": "assistant", "content": "Ciao, sono RagBot come posso aiutarti?"}]

    for msg in st.session_state.messagesU:
        st.chat_message(msg["role"],avatar="static/EVE.png").write(msg["content"])

    if prompt := st.chat_input(placeholder="Scrivi la tua richiesta..."):

        #client = OpenAI(api_key=openai_api_key)
        st.session_state.messagesU.append({"role": "user", "content": prompt})
        st.chat_message("user",avatar="static/user.png").write(prompt)
        msg=questionManagerP.ask_to_llm(prompt)
        dati=json.loads(msg)
        risposta = dati["risposta"]
        st.session_state.messagesU.append({"role": "assistant", "content": risposta})
        st.session_state["fonti-disponibili_u"] = "Si"
        
        #controller.set('fonti-trovate', msg)
        st.session_state['fonti_u'] = dati
        
        #controller.set('fonti-trovate', )
        st.chat_message("assistant",avatar="static/EVE.png").write(risposta)
        #Sintesi Vocale
        tts = gTTS(text=risposta, lang='it')
        tts.save('audio.mp3')
        audio_file = open('audio.mp3','rb')
        audio_bytes = audio_file.read()
        st.audio(audio_bytes, format='audio/mp3')
        audio_file.close()
        os.remove('audio.mp3')

        
        
        
