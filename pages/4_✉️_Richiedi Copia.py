###Librerie
import datetime as dt
import pandas as pd
import streamlit as st
import PyPDF2
from reportlab.lib.pagesizes import letter
from reportlab.lib import colors
from reportlab.lib.units import inch
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle,Image, Spacer, Paragraph
from reportlab.lib.styles import getSampleStyleSheet
from io import BytesIO
import smtplib
from email import encoders
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import json
from streamlit_lottie import st_lottie
import function as RagFunction

###Informazioni Pagina
st.set_page_config(
    page_title="Richiedi Copia",
    page_icon="✉️",
    layout="wide"
)

RagFunction.useCss("./style.css")
if "PdfCreato" not in st.session_state:
    st.session_state["PdfCreato"] = False

st.title("Richiedi Copia dei Messaggi della Sessione")


# Funzione per creare il PDF
def create_pdf(messages):
    buffer = BytesIO()

    image_path = "static/logoConText.png"
    image = Image(image_path, width=4*inch,height=0.9895*inch)

    styles = getSampleStyleSheet()
    styleN = styles['Normal']

    # Definisci le righe di intestazione del PDF
    table_data = [
        ["Sender",  "Message"]
    ]

    for msg in messages:
        # Crea un paragrafo per il messaggio per gestire il wrapping del testo
        msg_content = Paragraph(msg["content"].replace(".", ".<br/>"), styleN)
        table_data.append([msg["role"], msg_content])

    # Crea il documento PDF
    doc = SimpleDocTemplate(buffer, pagesize=letter)
    table = Table(table_data,colWidths=[2*inch, 6*inch])


    style = TableStyle([
        ('BACKGROUND', (0, 0), (-1, 0), colors.cornflowerblue),
        ('TEXTCOLOR', (0, 0), (-1, 0), colors.snow),
        ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
        ('FONTNAME', (0, 0), (-1, 0), 'Courier-Bold'),
        ('FONTSIZE', (0, 0), (-1, 0), 12),
        ('BOTTOMPADDING', (0, 0), (-1, 0), 12),
        ('BACKGROUND', (0, 1), (-1, -1), colors.whitesmoke),
        ('GRID', (0, 0), (-1, -1), 1, colors.black),
        ('FONTSIZE', (0, 0), (-1, 0), 12),
    ])
    # Applica lo stile alla tabella
    table.setStyle(style)
    #aggiungo un immagine al documento
    elements = [image, Spacer(1, 20), table]
    # Aggiungi la tabella al documento
    doc.build(elements)

    st.session_state["PdfCreato"] = True

    # Ritorna il contenuto del buffer
    return buffer.getvalue()

timestamp=dt.datetime.now()
email = st.secrets["EMAIL"]

subject = "Messaggi della sessione"
message = "Ciao! In allegato trovi i messaggi della sessione avvenuti in data:"+str(timestamp)+".\n\nGrazie e a presto!"+ "\n\nRaguy's Bot"
text = f"Subject: {subject}\n\n{message}"
server = smtplib.SMTP("smtp.gmail.com", 587)
server.starttls()
server.login(email, st.secrets["PASSWORD-EMAIL"])

# Carica i messaggi dalla sessione
if "messages" not in st.session_state:
    st.write("Non ci sono messaggi")
    with open("animazioni/animazione_non_trovato.json") as f:
        animazioneNonTrovato = json.load(f)
    st_lottie(animazioneNonTrovato , speed=1, height=600, key="non_trovato")
else:
    
    with open("animazioni/copyAnimation.json") as f:
        animazioneCopy = json.load(f)
    st_lottie(animazioneCopy , speed=1, height=300, key="copy")
    with st.spinner("Creazione PDF in corso..."):
                pdf_content = create_pdf(st.session_state.messages)
                # Salva il PDF su file
                with open('messages.pdf', 'wb') as f:
                    f.write(pdf_content)

                st.toast('PDF creato con successo', icon="✅")

    st.write("Ci sono messaggi")
    # Bottone per creare il PDF
    #dividi in 3 colonne
    col2, col3 = st.columns(2)
            
    with col2:
        if st.session_state["PdfCreato"]:
            st.download_button(label="Clicca qui per scaricare il PDF", data=open("messages.pdf", 'rb').read(), file_name="messages.pdf", mime="application/pdf")
    with col3:
        destinatario = st.text_input("Inserisci l'email")
        if destinatario != "":
            abilitazione_pulsante = False
        else: abilitazione_pulsante = True

        if st.button("Invia tramite email Email",disabled=abilitazione_pulsante):
            # Carica il PDF già creato
            with open('messages.pdf', 'rb') as f:
                pdf_data = f.read()

            # Crea l'oggetto MIME multipart
            msg = MIMEMultipart()
            msg['From'] = email
            msg['To'] = destinatario
            msg['Subject'] = subject
            # Crea l'oggetto MIMEBase per l'allegato PDF
            pdf_attachment = MIMEBase('application', 'pdf')
            pdf_attachment.set_payload(pdf_data)
            encoders.encode_base64(pdf_attachment)
            pdf_attachment.add_header('Content-Disposition', 'attachment; filename="messages.pdf"')

            # Allega il PDF al messaggio
            msg.attach(pdf_attachment)

            # Crea l'oggetto MIMEText per il corpo dell'email
            body = MIMEText(message, 'plain')
            msg.attach(body)

            # Invia l'email con l'allegato PDF
            server.sendmail(email, destinatario, msg.as_string())
            with open("animazioni/emailAnimation.json") as f:
                animazioneEmail = json.load(f)
            st_lottie(animazioneEmail , speed=1, height=300,loop=True)
            st.success("Email inviata con successo a: " + destinatario)


    
