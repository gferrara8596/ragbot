import pandas as pd
import plotly.express as px
import streamlit as st
from PIL import Image
import time
from streamlit_lottie import st_lottie
import json
import function as RagFunction

###Informazioni Pagina
st.set_page_config(
    page_title="RAGuisBot",
    page_icon="🤖",
    layout="wide"
)

RagFunction.useCss("styleHome.css")
RagFunction.useCss("style.css")

#centro il titolo usando le tre colonne
col1, col2, col3 = st.columns([2, 2, 2])
with col2:
    st.title("RAGuysBot")
with col3:
    RagFunction.animazione("animazioni/animazioneBot.json", height=200, key="bottino")


pathVideo = "static/demo1hd.mp4"


st.video(pathVideo, loop=True, autoplay=True, muted=True)



#with open("animazioni/animazione.json") as f:
    #animazione = json.load(f)

#st_lottie(animazione , speed=1, height=500, #key="initial")
# Markdown content with ID
markdown_text = st.markdown("""
<div id="legal-chatbot-info" style="text-shadow: 0 0 10px #5BC0BE;">

<div style="font-size: 24px;">This project was developed as part of the university course on Text Mining at the University of Naples Federico II, aiming to create a chatbot capable of answering legal questions. The chatbot's knowledge base is built upon hundreds of provided PDF documents. The project employs the RAG (Retrieval-Augmented Generation) approach to generate accurate and relevant responses. For text embedding, we use OpenAI's <span style="color: #5BC0BE; font-weight: bold;">`text-embedding-3-large`</span> model, and responses are generated using <span style="color: #5BC0BE; font-weight: bold;">`GPT-3.5-Turbo-0125`</span>. Voice cloning is achieved through <span style="color: #5BC0BE; font-weight: bold;">`ElevenLabs' APIs`</span>.</div>
    

### Key Features:
- **`Chatbot with Predefined Knowledge Base:`** Answers questions using the provided documents.
- **`Source Visualization:`** Allows users to see the document from which the response originated.
- **`Chat History Download and Email:`** Enables users to download and email the chat history.
- **`Personal Chatbot:`** Answers questions based on documents uploaded by the user.
- **`File Statistics:`** Provides detailed statistics on the analyzed documents.
- **`Text to Speech and Speech to Text:`** Converts between text and speech.
- **`Voice Synthesizers:`** Utilizes Google voice synthesizers and cloned voices for responses via ElevenLabs' APIs.

</div>
""", unsafe_allow_html=True)



#centro il bottone usando le tre colonne
col1, col2, col3 = st.columns([2, 2, 2])


with col2:
    if st.button("Reset cache session", type="primary"):
        st.session_state.clear()
        with st.spinner('Wait for it...'):
            time.sleep(2)
            st.success('Done!')
            time.sleep(2)
            st.rerun()
