import os
import bs4
__import__('pysqlite3') 
import sys 
sys.modules['sqlite3'] = sys.modules.pop('pysqlite3')
from langchain_community.document_loaders.pdf import PyPDFium2Loader
from langchain_chroma import Chroma
from langchain_openai import OpenAIEmbeddings
from langchain_text_splitters import RecursiveCharacterTextSplitter
from langchain_core.prompts import PromptTemplate
from langchain_openai import ChatOpenAI
from langchain_core.output_parsers import StrOutputParser
from langchain_core.runnables import RunnablePassthrough
from itertools import chain
import json
import streamlit as st


class LLMQuestionManager:

    def __init__(self, files: list[str],chunk_size=500,chunk_overlap=100,persistent_directory="embeddings") -> None:
        self.files = files
        self.chunk_size = chunk_size
        self.chunck_overlap = chunk_overlap
        self.persistent_directory = persistent_directory
        self.apiKey =  st.secrets["OPENAI_API_KEY"]
        self.vectorstore = self.embedding_text()
        pass
    
    
    #Caricamento documenti PDF, li suddivide in parti più piccole e crea embeddings."""
    def embedding_text(self):

        docArray = []
        #caricamento e suddivisione documenti PDF
        for file in self.files:
            loader = PyPDFium2Loader(file_path=file)
            doc = loader.load()
            text_splitter = RecursiveCharacterTextSplitter(
            chunk_size=self.chunk_size, chunk_overlap=self.chunck_overlap, separators=["\n\n", "\n", " ", ""]
        )
            all_splits = text_splitter.split_documents(doc)
            docArray.append(all_splits)

        documents = list(chain.from_iterable(docArray))

        #creazione embeddings con OpenAI
        embeddings = OpenAIEmbeddings(model="text-embedding-3-large",show_progress_bar=True, api_key=self.apiKey)

        #caricamento embeddings già esistenti o creazione di nuovi embeddings
        if os.path.exists(self.persistent_directory):
            print("Caricamento embeddings già esistenti...")
            vectorstore = Chroma(persist_directory=self.persistent_directory, embedding_function=embeddings)
        else:
            
            vectorstore = Chroma.from_documents(documents=documents, embedding=embeddings, persist_directory=self.persistent_directory)
        return vectorstore

    #Interrogazione modello LLM con una domanda e restituisce la risposta insieme alle fonti.
    def ask_to_llm(self,question: str):
        
        #Creazione di un retriever per i documenti basato sulla similarità
        retriever = self.vectorstore.as_retriever(search_type="similarity_score_threshold",search_kwargs={'score_threshold': 0.7})
        retrieved_docs = retriever.invoke(question)
        print("tentativo 0.7")

        #Se non vengono trovati documenti con una similarità maggiore di 0.7, si abbassa il threshold (soglia di similarità)
        if len(retrieved_docs) == 0:
            retriever = self.vectorstore.as_retriever(search_type="similarity_score_threshold",search_kwargs={'score_threshold': 0.5})
            retrieved_docs = retriever.invoke(question)
            print("tentativo 0.5")
        
        #Se ancora non vengono trovati risultati, prendi i primi 10 documenti
        if len(retrieved_docs) == 0:
            retriever = self.vectorstore.as_retriever(search_kwargs={'k': 10})
            retrieved_docs = retriever.invoke(question)
            print("tentativo primi 10 elementi")

        print("Di seguito le fonti della risposta")

        #Salva le informazioni delle fonti
        sources_data = []

        for doc in retrieved_docs:
            source_data = {
                "testo": doc.page_content,
                "pagina": doc.metadata["page"],
                "documento": doc.metadata["source"]
            }
            sources_data.append(source_data)

            print("testo -> \n", doc.page_content, "\n")
            print("pagina : ", doc.metadata["page"], "\n", "documento : ", doc.metadata["source"], "\n\n")
        
        sources_json = json.dumps(sources_data, indent=4)
        
        #Configurazione modello di linguaggio OpneAI
        llm = ChatOpenAI(model="gpt-3.5-turbo-0125", api_key=self.apiKey)

        #Creazione di un contesto per la domanda
        template = """Use only the following pieces of context to answer the question at the end.
    If you don't know the answer, just say that you don't know, don't try to make up an answer.
    try to be exhaustive and if possible also cite pieces of the text from which you took the information.
    Use at least 500 characters to answer.
    Reply in italian.
    {context}
    Question: {question}
    Helpful Answer:"""

        prompt = PromptTemplate.from_template(template)

        #Esempio di messaggi per il prompt
        example_messages = prompt.invoke(
        {"context": "filler context", "question": "filler question"}
    ).to_messages()

        #prende una lista di documenti e combina i loro contenuti di pagina in una singola stringa, separando ogni contenuto con due ritorni a capo.
        def format_docs(docs):
            return "\n\n".join(doc.page_content for doc in docs)
        
        # Crea una catena di esecuzione per RAG (Retrieval-Augmented Generation)
        #1. Recupera e formatta i documenti
        #2. Combina il contesto formattando con la domanda in un dizionario.
        #3. Passa il contesto e la domanda al prompt.
        #4. Passa il dizionario al modello di linguaggio.
        #5. Converte l'output del modello in una stringa utilizzando un parser di output.
        rag_chain = (
        {"context": retriever | format_docs, "question": RunnablePassthrough()}
        | prompt
        | llm
        | StrOutputParser()
    )
        #esecuzione catena per ottenere una risposta
        response = ""
        for chunk in rag_chain.stream(question):
            response += chunk
            
        #combina le fonti e la risposta in un unico JSON
        final_json_data = {
            "fonti": sources_data,
            "risposta": response
        }

        final_json = json.dumps(final_json_data, indent=4)

        return final_json



