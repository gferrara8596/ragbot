import streamlit as st
import json
import os
from streamlit_lottie import st_lottie

@st.cache_data
def useCss(fileStylePath):
    with open(fileStylePath, 'r') as f:
        css = f.read()
    st.markdown(f'<style>{css}</style>', unsafe_allow_html=True)

def animazione(animazionePath, height=None, key=None):
    with open(animazionePath) as f:
        animazione = json.load(f)
        if (height is None)and (key is None):
            st_lottie(animazione , speed=1, quality="medium")
        elif (height is None):
            st_lottie(animazione , speed=1, key=key,quality="medium")
        elif (key is None):
            st_lottie(animazione , speed=1, height=height,quality="medium")
        else:
            st_lottie(animazione , speed=1, height=height, key=key,quality="medium")

