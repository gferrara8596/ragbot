def comment_lines_in_file(filename, patterns):
    # Legge il contenuto del file
    with open(filename, 'r') as file:
        lines = file.readlines()

    # Commenta le righe che contengono i pattern specificati
    with open(filename, 'w') as file:
        for line in lines:
            # Verifica se la riga contiene un pattern e non è già commentata
            if any(pattern in line and not line.strip().startswith('#') for pattern in patterns):
                line = '# ' + line
            file.write(line)

if __name__ == "__main__":
    # File da modificare
    filename = 'rag.py'
    # Pattern da cercare
    patterns = ["__import__('pysqlite3')", "sys.modules['sqlite3'] = sys.modules.pop('pysqlite3')"]

    # Commenta le righe nel file
    comment_lines_in_file(filename, patterns)
