@echo off

setlocal enabledelayedexpansion

set "input_file=requirements.txt"
set "temp_file=temp.txt"

if exist %temp_file% del %temp_file%

for /f "delims=" %%i in (%input_file%) do (
    set "line=%%i"
    echo !line! | findstr /i /v "uvloop==0.19.0" | findstr /i /v "pysqlite3-binary==0.5.2.post3" >> %temp_file%
)

move /y %temp_file% %input_file%

endlocal


python .\comment.py

pip install virtualenv

virtualenv ambiente_virtuale

call ambiente_virtuale\Scripts\activate

if not exist ".\ambiente_virtuale\ok_requirements.txt" (
    pip install -r requirements.txt
    echo success > ".\ambiente_virtuale\ok_requirements.txt"
)

setlocal enabledelayedexpansion

set "directory=.streamlit"
set "secrets_file=%directory%\secrets.toml"

if not exist "%secrets_file%" (
    set /p "api_key=Inserisci la tua OPENAI_API_KEY: "
    set /p "email=Inserisci la tua EMAIL: "
    set /p "password=Inserisci la tua PASSWORD-EMAIL: "

    (
        echo OPENAI_API_KEY = "!api_key!"
        echo EMAIL = "!email!"
        echo PASSWORD-EMAIL = "!password!"
    ) > "%secrets_file%"


    echo File secrets.toml creato con successo nella cartella %directory%.
) else (
    echo Il file secrets.toml esiste gia' nella cartella %directory%.
)

echo streamlit run .\1_Home.py > run.bat
echo e' stato correttamente creato il file run.bat, eseguilo per avviare streamlit

pause
