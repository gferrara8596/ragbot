# RAGBot

![Intro](Screen/demo.gif)

This project was developed as part of the university course on Text Mining at the University of Naples Federico II, aiming to create a chatbot capable of answering legal questions. The chatbot's knowledge base is built upon hundreds of provided PDF documents. The project employs the RAG (Retrieval-Augmented Generation) approach to generate accurate and relevant responses. For text embedding, we use OpenAI's `text-embedding-3-large` model, and responses are generated using `GPT-3.5-Turbo-0125`. Voice cloning is achieved through `ElevenLabs' APIs`.

# Project
![Modello](static/modello.png)

### Key Features:
- **`Chatbot with Predefined Knowledge Base:`** Answers questions using the provided documents.
- **`Source Visualization:`** Allows users to see the document from which the response originated.
- **`Chat History Download and Email:`** Enables users to download and email the chat history.
- **`Personal Chatbot:`** Answers questions based on documents uploaded by the user.
- **`File Statistics:`** Provides detailed statistics on the analyzed documents.
- **`Text to Speech and Speech to Text:`** Converts between text and speech.
- **`Voice Synthesizers:`** Utilizes Google voice synthesizers and cloned voices for responses via ElevenLabs' APIs⚠️*.

  *⚠️An ElevenLabs service was used to clone an entry, none of the project authors take responsibility for any misuse of this feature,
  use ElevenLabs responsibly.
  Disclosure is for academic research purposes only.

## Demo Streamlit

[![Streamlit](https://static.streamlit.io/badges/streamlit_badge_black_white.svg)](https://raguys-chatbot.streamlit.app/) 

[![YouTube](https://img.shields.io/badge/YouTube-%23FF0000.svg?style=for-the-badge&logo=YouTube&logoColor=white)](https://github.com/ClaudioDotani/Progetto-Text-Mining-Chatbot/raw/main/static/demo1hd.mp4)

## Screen

**RAGBot**
![image](https://github.com/ClaudioDotani/Progetto-Text-Mining-Chatbot/assets/101301398/afaf88e0-0a4d-4594-8b3b-3a5e9b44db2c)

**Source of Documents**
![image](https://github.com/ClaudioDotani/Progetto-Text-Mining-Chatbot/assets/101301398/61ff36fd-0ae1-4076-b3f1-5c6a4a3854b3)

**Request a copy of the conversation**
![image](https://github.com/ClaudioDotani/Progetto-Text-Mining-Chatbot/assets/101301398/936b7b32-f43d-4af9-ae8c-08e1dcf618cd)

**Demo pdf format response**
![image](https://github.com/ClaudioDotani/Progetto-Text-Mining-Chatbot/assets/101301398/41e10abc-84f5-45a1-8147-111a3251dfbd)

**Personal ChatBot**
![image](https://github.com/ClaudioDotani/Progetto-Text-Mining-Chatbot/assets/101301398/e91ec1bf-6fd9-45b6-b6b6-84eacc36d5e9)

**Statistics**
![image](https://github.com/ClaudioDotani/Progetto-Text-Mining-Chatbot/assets/101301398/1b5fd937-84e0-41d3-a8af-55c50da42bae)

**Contacts**
![image](https://github.com/ClaudioDotani/Progetto-Text-Mining-Chatbot/assets/101301398/661e2557-6e09-4954-9750-bca4ebac6927)






## Local Installation

To run the project locally, follow these steps:

1. **Clone the repository**:
    ```sh
    git clone https://github.com/ClaudioDotani/Progetto-Text-Mining-Chatbot
    cd Progetto-Text-Mining-Chatbot
    ```

2. **Create and activate a virtual environment**:
    ```sh
    python -m venv ambiente_virtuale
    source `ambiente_virtuale/bin/activate`   On Windows use `.\ambiente_virtuale\Scripts\Activate.ps1`
    ```

3. **⚠️`Only on Windows`, in the requirements.txt file remove uvloop==0.19.0 and pysqlite3-binary==0.5.2.post3⚠️**
   ```sh
    delete or comment in requirements.txt
    #uvloop==0.19.0
    #pysqlite3-binary==0.5.2.post3
    These packages are only useful for deployment!
    ```

5. **Install the required dependencies**:
    ```sh
    pip install -r requirements.txt
    ```
6. **⚠️Comment `rag.py` file⚠️**
   ```sh
   comment in RAG file
   #import __import__('pysqlite3')
   #import sys.modules['sqlite3'] = sys.modules.pop('pysqlite3')
   on line 3 and 5.
   These imports are only useful for deployment!
   ```
7. **Create a `secrets.toml` file formatted like this in the /Project-Text-Mining-Chatbot/.streamlit directory**
   ```sh
    OPENAI_API_KEY = "YOUR_API_KEY"
    EMAIL = "YOUR_EMAIL"
    PASSWORD-EMAIL = "YOUR_PASSWORD"
   ```

8. **Run the Streamlit app**:
    ```sh
    streamlit run .\1_Home.py
    ```
    ***
    As an alternative to the steps above, `on Windows only`, you can use the [settings.bat](https://github.com/ClaudioDotani/Progetto-Text-Mining-Chatbot/blob/main/settings.bat) 
    file, which will do all the steps for you. 
    It will generate a run.bat file that will allow you to start streamlit locally, bypassing all the steps described above.

   From cmd run the command:
    ```sh
    git clone https://github.com/ClaudioDotani/Progetto-Text-Mining-Chatbot
    cd Progetto-Text-Mining-Chatbot
    ```
    and click on settings.bat, wait for execution and then on run.bat
      

## Usage

### Chatbot with Predefined Knowledge Base
1. Open the Streamlit app.
2. Enter your legal question in the input box.
3. View the response along with the source document.

### Custom Document Chatbot
1. Upload your document in the designated section of the app.
2. Ask your question based on the uploaded document.
3. Receive and view the response along with the source information.

### Chat Management
- Download the chat history as a PDF file.
- Email the chat history directly from the app.

### Text-to-Speech and Speech-to-Text
- Convert text responses to speech using Google TTS.
- Clone Voice using ElevenLabs API.
- Use speech input to ask questions.

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.

## Acknowledgements

We would like to thank the following resources and technologies for their support:

- **OpenAI** for the `text-embedding-3-large` and `GPT-3.5-Turbo-0125` models.
- **ElevenLabs** for voice cloning APIs.
- **Google** for text-to-speech and speech-to-text APIs.
- **Streamlit** for providing an easy-to-use web application framework.

## Contact

For any questions or inquiries, please contact us at:

- **Email**: [claudiodotani@gmail.com](mailto:claudiodotani@gmail.com)
- **Email**: [mauro.galateo.996@gmail.com](mailto:mauro.galateo.996@gmail.com)
- **Email**: [gferrara8596@gmail.com](mailto:gferrara8596@gmail.com)
  ### GitHub Profiles
- **Claudio Dotani:** [@ClaudioDotani](https://github.com/ClaudioDotani)
- **Mauro Galateo:** [@mgalateo](https://github.com/mgalateo)
- **Giuseppe Ferrara:** [@gferrara8596](https://github.com/gferrara8596)
  
- **GitHub Issues**: [GitHub Issues Page](https://github.com/ClaudioDotani/Progetto-Text-Mining-Chatbot/issues)

Feel free to reach out with any questions, suggestions, or feedback!

---

We hope you find this project useful and educational. Enjoy exploring the capabilities of our legal chatbot!

